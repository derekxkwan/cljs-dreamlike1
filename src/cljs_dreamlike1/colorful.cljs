(ns cljs-dreamlike1.colorful
  (:require [quil.core :as q :include-macros true]))

(def cur-dim {:w nil :h nil})

(def my-colors
  (map (partial apply q/color)
       '[(252 125 225)
         (126 246 252)
         (245 252 143)
         (252 143 143)
         (192 252 143)
         (252 209 143)]))

(def colors-n (count my-colors))

(def color-idxer (atom (range my-colors)))
         
(def color-div nil)

(def shuffle-time 60)

(def cur-time (atom 0
                    :validator #(>= % 0))
  )

;; width of color bar normalized
(def color-width 0.7)

(defn setup [w h]
  (let [new-gr (q/create-graphics w h :p2d)]
    (set! cur-dim {:w w :h h})
    (set! color-div (map #(quot (* % w) colors-n) (range colors-n)))
    new-gr
    ))

(defn update []
  (when (= @cur-time 0)
    (swap! color-idxer shuffle))
  (swap! cur-time #(mod (inc %) shuffle-time))
  )
  

(defn draw []
  (q/no-fill)
  (let [w (get cur-dim :w)
        h (get cur-dim :h)]
    (loop [i 0]
      (let [i-map (/ (* i colors-n) colors-n)
            idx-map (int i-map)
            color1-idx (get @color-idxer idx-map)
            color2-idx (get @color-idxer (min (dec colors-n) (inc color1-idx)))
            i-map-constr (min ((partial max color-width) (mod i-map 1)) 1)
            interp (q/map-range i-map-constr color-width 1 0 1)
            cur-color (q/lerp-color (get my-colors color1-idx) (get my-colors color2-idx) interp)]
        (q/stroke cur-color)
        (q/line i 0 i h)
        (when (< i (dec w)) (recur (inc i)))
        ))
    ))
            
        
      
  
