(ns cljs-dreamlike1.core
  (:require [quil.core :as q :include-macros true]
            [cljs-dreamlike1.colorful :as col]
            [quil.middleware :as m]))

(def cur-dim {:w 1024 :h 768})

(defn setup []
  ; Set frame rate to 30 frames per second.
  (q/frame-rate 30)
  ; Set color mode to HSB (HSV) instead of default RGB.
  ; setup function returns initial state. It contains
  ; circle color and position.
  {:colorful (col/setup)}
    )

(defn update-state [state]
  ; Update sketch state by changing circle color and position.
  )

(defn draw-state [state]
  ; Clear the sketch by filling it with light-grey color.
  (q/background 240)

  

  )

; this function is called in index.html
(defn ^:export run-sketch []
  (q/defsketch cljs-dreamlike1
    :host "cljs-dreamlike1"
    :size [(get cur-dim :w) (get cur-dim :h)]
    ; setup function called only once, during sketch initialization.
    :setup setup
    ; update-state is called on each iteration before draw-state.
    :update update-state
    :draw draw-state
    ; This sketch uses functional-mode middleware.
    ; Check quil wiki for more info about middlewares and particularly
    ; fun-mode.
    :middleware [m/fun-mode])
  )

; uncomment this line to reset the sketch:
; (run-sketch)
